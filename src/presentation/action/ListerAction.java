/**
 * 
 */
package presentation.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.UtilisateurDto;

/**
 * Classe pour lister les utilisateurs
 *
 * @author Mirailov Date : 29 mars 2021
 */
public class ListerAction extends Action {

	/**
	 * constante pour avoir la clef de la map
	 */
	public static final String LISTER_REQUEST = "mapUtilisateurRequest";

	/**
	 * Constructor ListerAction
	 */
	public ListerAction() {
		super();
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) {
		@SuppressWarnings("unchecked")
		final Map<Integer, UtilisateurDto> mapUtilisateur = (Map<Integer, UtilisateurDto>) request.getSession()
				.getAttribute(InitAction.MAP_UTILISATEUR_SESSION);
		request.setAttribute(LISTER_REQUEST, mapUtilisateur);
		return mapping.findForward("success");
	}

}
