/**
 * 
 */
package presentation.action;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.RedirectingActionForward;

/**
 * Classe changeant la langue
 *
 * @author Mirailov Date : 31 mars 2021
 */
public class LangueAction extends Action {

	/**
	 * Constructor LangueAction
	 */
	public LangueAction() {
		super();
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) {
		final String referer = request.getHeader("referer");
		final String language = request.getParameter("locale");
		final HttpSession session = request.getSession();

		if ("EN".equals(language)) {
			session.setAttribute(Globals.LOCALE_KEY, Locale.ENGLISH);
		} else {
			session.setAttribute(Globals.LOCALE_KEY, Locale.FRENCH);
		}

		if (referer != null) {
			// redirection vers l'url du referrer.
			final ActionForward forward = new RedirectingActionForward();
			forward.setPath(referer);
			return forward;
		}
		// redirection vers une page d�finie par d�faut
		return mapping.findForward("success");
	}

}
