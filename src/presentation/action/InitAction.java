/**
 * 
 */
package presentation.action;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.UtilisateurDto;

/**
 * Classe faisant l'initialisation de la map des utilisateurs
 *
 * @author Mirailov Date : 30 mars 2021
 */
public class InitAction extends Action {

	/**
	 * constante map utilisateur
	 */
	public static final String MAP_UTILISATEUR_SESSION = "mapUtilisateurSession";

	/**
	 * Constructor InitAction
	 */
	public InitAction() {
		super();
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) {

		// cr�er les utilisateur
		final Map<Integer, UtilisateurDto> mapUtilisateur = this.initialiserUtilisateur();

		// On r�cup�re la session
		final HttpSession session = request.getSession();

		// On y ajoute les users et la map
		session.setAttribute(MAP_UTILISATEUR_SESSION, mapUtilisateur);
		session.setAttribute(Globals.LOCALE_KEY, Locale.FRANCE);

		return mapping.findForward("success");
	}

	/**
	 * Permet de cr�er la map des utilisateurs
	 *
	 * @return Map<Integer, UtilisateurDto> la map des utilisateurs
	 */
	private Map<Integer, UtilisateurDto> initialiserUtilisateur() {
		final Map<Integer, UtilisateurDto> mapPersonne = new HashMap<>();
		final UtilisateurDto utilisateur1 = (new UtilisateurDto()).initUtilisateur(1, "Sylver", "Waks", 45);
		final UtilisateurDto utilisateur2 = (new UtilisateurDto()).initUtilisateur(4, "Saint", "Seya", 25);
		final UtilisateurDto utilisateur3 = (new UtilisateurDto()).initUtilisateur(12, "Tsuno", "Yoko", 35);
		mapPersonne.put(utilisateur1.getId(), utilisateur1);
		mapPersonne.put(utilisateur2.getId(), utilisateur2);
		mapPersonne.put(utilisateur3.getId(), utilisateur3);
		return mapPersonne;
	}

}
