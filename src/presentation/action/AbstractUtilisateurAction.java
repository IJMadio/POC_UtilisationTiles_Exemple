/**
 * 
 */
package presentation.action;

import java.util.Map;

import org.apache.struts.action.Action;

import presentation.bean.UtilisateurDto;

/**
 * Classe abstraite pour g�rer les utilisateurs
 *
 * @author Mirailov
 * Date : 30 mars 2021
 *
 */
public abstract class AbstractUtilisateurAction extends Action {
	/**
	 * Permet de retourner un utilisateur selon son id
	 *
	 * @param  id             l'identifiant de l'utilisateur
	 * @param  mapUtilisateur la map contenant les utilisateurs
	 * @return                Utilisateur l'utilisateur trouv�
	 */
	protected UtilisateurDto findUtilisateur(final Integer id, final Map<Integer, UtilisateurDto> mapUtilisateur) {
		return mapUtilisateur.get(id);
	}

	/**
	 * Permet de supprimer un utilisateur
	 *
	 * @param  id             l'utilisateur � enlever
	 * @param  mapUtilisateur la map des utilisateurs
	 * @return                l'utilisateur supprimer de la map
	 */
	protected UtilisateurDto removeUser(final Integer id, final Map<Integer, UtilisateurDto> mapUtilisateur) {
		return mapUtilisateur.remove(id);
	}
}
