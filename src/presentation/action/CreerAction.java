/**
 * 
 */
package presentation.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import presentation.bean.UtilisateurDto;
import presentation.form.UtilisateurForm;

/**
 * Classe pour ajouter un utilisateur � la map ou list.
 *
 * @author Mirailov Date : 31 mars 2021
 */
public class CreerAction extends AbstractUtilisateurAction {

	/**
	 * Constructor CreerAction
	 */
	public CreerAction() {
		super();
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) {
		final UtilisateurForm userForm = (UtilisateurForm) form;
		@SuppressWarnings("unchecked")
		final Map<Integer, UtilisateurDto> mapUtilisateur = (Map<Integer, UtilisateurDto>) request.getSession()
				.getAttribute(InitAction.MAP_UTILISATEUR_SESSION);
		final Integer idUtilisateur = Integer.valueOf(userForm.getId());
		if (this.findUtilisateur(idUtilisateur, mapUtilisateur) != null) {
			final ActionErrors errors = new ActionErrors();
			errors.add("id", new ActionMessage("errors.id.existant"));
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("errors.itHasError.message"));
			saveErrors(request, errors);
			return mapping.findForward("error");
		}
		final String nomUtilisateur = userForm.getNom();
		final String prenomUtilisateur = userForm.getPrenom();
		final int ageUtilisateur = Integer.valueOf(userForm.getAge());

		final UtilisateurDto utilisateur = (new UtilisateurDto()).initUtilisateur(idUtilisateur, nomUtilisateur, prenomUtilisateur,
				ageUtilisateur);
		mapUtilisateur.put(idUtilisateur, utilisateur);

		final ActionMessages messages = new ActionMessages();
		messages.add("userOK", new ActionMessage("ok.user.ajout", new Object[] { userForm.getId() }));
		saveMessages(request, messages);

		return mapping.findForward("success");
	}

}
