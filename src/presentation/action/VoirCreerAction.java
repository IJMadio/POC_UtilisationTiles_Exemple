/**
 * 
 */
package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Classe Action pour voir le formulaire
 *
 * @author Mirailov Date : 31 mars 2021
 */
public class VoirCreerAction extends Action {

	/**
	 * Constructor VoirCreerAction
	 */
	public VoirCreerAction() {
		super();
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) {
		return mapping.findForward("success");
	}
}
