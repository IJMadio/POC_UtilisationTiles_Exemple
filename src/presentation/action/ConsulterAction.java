/**
 * 
 */
package presentation.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.UtilisateurDto;

/**
 * Classe �tendant Action afin d'afficher les d�tails d'un utilisateur.
 *
 * @author Mirailov Date : 29 mars 2021
 */
public class ConsulterAction extends AbstractUtilisateurAction {

	/**
	 * constante sp�cifiant le nom de la request utilis�e pour un utilisateur
	 */
	public static final String UTILISATEUR_REQUEST = "utilisateurRequest";

	/**
	 * Constructor ConsulterAction
	 */
	public ConsulterAction() {
		super();
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) {
		@SuppressWarnings("unchecked")
		final Map<Integer, UtilisateurDto> mapUtilisateur = (Map<Integer, UtilisateurDto>) request.getSession()
				.getAttribute(InitAction.MAP_UTILISATEUR_SESSION);

		final Integer id = Integer.valueOf(request.getParameter("identifiant"));

		final UtilisateurDto utilisateur = this.findUtilisateur(id, mapUtilisateur);

		request.setAttribute(UTILISATEUR_REQUEST, utilisateur);

		return mapping.findForward("success");
	}
}
