/**
 * 
 */
package presentation.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.UtilisateurDto;

/**
 * Classe pour supprimer un utilisateur
 *
 * @author Mirailov Date : 30 mars 2021
 */
public class SupprimerAction extends AbstractUtilisateurAction {

	/**
	 * Constructor SupprimerAction
	 */
	public SupprimerAction() {
		super();
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) {
		final Integer idUtilisateur = Integer.valueOf(request.getParameter("id"));
		final HttpSession session = request.getSession();

		@SuppressWarnings("unchecked")
		final Map<Integer, UtilisateurDto> mapUtilisateur = (Map<Integer, UtilisateurDto>) session
				.getAttribute(InitAction.MAP_UTILISATEUR_SESSION);
		this.removeUser(idUtilisateur, mapUtilisateur);
		return mapping.findForward("success");
	}

}
