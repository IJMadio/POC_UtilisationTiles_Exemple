/**
 * 
 */
package presentation.form;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * Classe faisant la liaison avec les formulaires
 *
 * @author Mirailov Date : 31 mars 2021
 */
public class UtilisateurForm extends ActionForm {

	private static final long serialVersionUID = 328736578963851111L;

	private String            id;
	private String            nom;
	private String            prenom;
	private String            age;

	/**
	 * Constructor UtilisateurForm
	 */
	public UtilisateurForm() {
		super();
	}

	@Override
	public void reset(final ActionMapping mapping, final HttpServletRequest request) {
		this.id = null;
		this.nom = null;
		this.prenom = null;
		this.age = null;
		super.reset(mapping, request);
	}

	@Override
	public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
		final ActionErrors errors = new ActionErrors();
		final String pattern = "[1-9][0-9]*";
		if (id.isEmpty()) {
			errors.add("id", new ActionMessage("errors.id.obligattoire"));
		}
		if (!Pattern.matches(pattern, id)) {
			errors.add("id", new ActionMessage("errors.id.numerique"));
		}
		if (nom.isEmpty()) {
			errors.add("nom", new ActionMessage("errors.nom.obligattoire"));
		}
		if (prenom.isEmpty()) {
			errors.add("prenom", new ActionMessage("errors.prenom.obligattoire"));
		}
		if (age.isEmpty()) {
			errors.add("age", new ActionMessage("errors.age.obligattoire"));
		}
		if (!Pattern.matches(pattern, age)) {
			errors.add("age", new ActionMessage("errors.age.numerique"));
		}
		if (errors.size() > 0) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("errors.itHasError.message"));
		}
		return errors;
	}

	/**
	 * Getter for id
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Setter for id
	 *
	 * @param id the id to set
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Getter for nom
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Setter for nom
	 *
	 * @param nom the nom to set
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * Getter for prenom
	 *
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Setter for prenom
	 *
	 * @param prenom the prenom to set
	 */
	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Getter for age
	 *
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * Setter for age
	 *
	 * @param age the age to set
	 */
	public void setAge(final String age) {
		this.age = age;
	}
}
