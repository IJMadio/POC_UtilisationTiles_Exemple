package presentation.bean;

import java.io.Serializable;

/**
 * Classe repr�sentant un Utilisateur
 * 
 * @author  xsintive
 * @version 1.0
 */
public class UtilisateurDto implements Serializable {

	private static final long serialVersionUID = 3654297383714722490L;
	
	private int               id;
	private String            nom;
	private String            prenom;
	private int               age;

	/**
	 * Constructeur par d�faut
	 */
	public UtilisateurDto() {
		// empty method
	}

	/**
	 * Permet d'initialiser un utilisateur
	 * 
	 * @param  id     l'id de l'utilisateur
	 * @param  nom    le nom de l'utilisateur
	 * @param  prenom le prenom de l'utilisateur
	 * @param  age    l'age de l'utilisateur
	 * @return        l'utilisateur initialis�
	 */
	public UtilisateurDto initUtilisateur(final int id, final String nom, final String prenom, final int age) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		return this;
	}

	/**
	 * Getter for id
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter for id
	 *
	 * @param id the id to set
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Getter for nom
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Setter for nom
	 *
	 * @param nom the nom to set
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * Getter for prenom
	 *
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Setter for prenom
	 *
	 * @param prenom the prenom to set
	 */
	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Getter for age
	 *
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Setter for age
	 *
	 * @param age the age to set
	 */
	public void setAge(final int age) {
		this.age = age;
	}
}
