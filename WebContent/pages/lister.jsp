<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="presentation.action.ListerAction"%>
<%@ page import="presentation.bean.UtilisateurDto"%>
<h1><bean:message key="lister.titre" /></h1>
<ul>
	<logic:iterate name="<%=ListerAction.LISTER_REQUEST%>" id="utilisateur">
		
		<%-- lien avec de EL --%>
		<li>(EL) <bean:message key="lister.nom" /> : <html:link href="consulter.do?identifiant=${utilisateur.key}">${utilisateur.value.nom}</html:link></li>
		
		<%-- lien sans EL --%>
		<li>(NEL) <bean:message key="lister.nom" /> :
		<html:link href="consulter.do" paramId="identifiant" paramName="utilisateur" paramProperty="key">
		<%-- si la property n'est pas un String, il faut pr�ciser le format --%>
		<bean:write name="utilisateur" property="key" format="00"/> - <bean:write name="utilisateur" property="value.nom"/>
		</html:link>
		</li>
	
	</logic:iterate>
</ul>
