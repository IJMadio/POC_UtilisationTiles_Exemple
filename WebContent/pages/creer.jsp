<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<h1><bean:message key="creer.titre" /></h1>
<br/>
<html:errors property="org.apache.struts.action.GLOBAL_MESSAGE" header="" prefix="" footer="" suffix=""/>
<html:form action="/creer.do" focus="id">
	<table style="border: 0px">
		<tr>
			<td align="right"><label for="id"><bean:message key="creer.id"/> :</label></td>
			<td align="left"><html:text property="id" size="20" maxlength="20"/></td>
			<td><html:errors property="id" /></td>
		</tr>
		<tr>
			<td align="right"><label for="nom"><bean:message key="creer.nom"/> :</label></td>
			<td align="left"><html:text property="nom" size="20" maxlength="20"/></td>
			<td><html:errors property="nom" /></td>
		</tr>
		<tr>
			<td align="right"><label for="prenom"><bean:message key="creer.prenom"/> :</label></td>
			<td align="left"><html:text property="prenom" size="20" maxlength="20"/></td>
			<td><html:errors property="prenom" /></td>
		</tr>
		<tr>
			<td align="right"><label for="age"><bean:message key="creer.age"/> :</label></td>
			<td align="left"><html:text property="age" size="20" maxlength="20"/></td>
			<td><html:errors property="age" /></td>
		</tr>
		<tr>
			<td align="right"><bean:define id="creerSubmit"><bean:message key="creer.submit"/></bean:define><html:submit property="submit" value="${creerSubmit}"/></td>
			<td align="left"><html:reset /></td>
			<td></td>
		</tr>
	</table>
</html:form>
<%-- permet d'afficher toutes les erreurs --%>
<%-- <html:errors /> --%>
<%-- Permet d'afficher les messages si present --%>
<logic:messagesPresent message="true">
	<html:messages id="userOK" message="true" header="myForm.header" footer="myForm.footer">
		<bean:write name="userOK" />
	</html:messages>
</logic:messagesPresent>