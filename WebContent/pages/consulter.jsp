<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<h1><bean:message key="consulter.titre" /></h1>
<ul>
	<li><bean:message key="consulter.nom" /> : <bean:write name="utilisateurRequest" property="nom"/></li>
	<li><bean:message key="consulter.prenom" /> : <bean:write name="utilisateurRequest" property="prenom"/></li>
	<li><bean:message key="consulter.age" /> : <bean:write name="utilisateurRequest" property="age"/></li>
	<li><html:link href="supprimer.do?id=${utilisateurRequest.id}"><html:button property="true"><bean:message key="consulter.supprimer" /></html:button></html:link></li>
</ul>
