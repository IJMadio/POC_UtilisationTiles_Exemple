<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html lang="fr" style="width:100%; height: 100%; margin:0; padding: 0;">
<head>
<meta charset="ISO-8859-1">
<title><tiles:getAsString name="title" ignore="true" /></title>
</head>
<body style="width:100%; height: 100%; margin:0; padding: 0;">
	<header style="width: 100%; height: 20%; margin: 0; padding: 0; display: block; background-color: orange; color : white;">
		<tiles:insert attribute="header" />
	</header>
	<aside style="width: 19.7%; height: 60%; margin: 0; padding: 0; display: inline-block; background-color: green; color : white; vertical-align:bottom;">
		<tiles:insert attribute="menu"/>
	</aside>
	<section style="width: 80%; height: 60%; margin: 0; padding: 0; display: inline-block; background-color: brown; color : white; vertical-align:bottom;">
		<tiles:insert attribute="body" />
	</section>
	<footer style="width: 100%; height: 20%; margin: 0; padding: 0; display: block; background-color: red; color : white;">
		<tiles:insert attribute="footer" />
	</footer>
</body>
</html>